﻿using System;
using HRM.Model.Entities.Employees;
using HRM.Model.Entities.Shifts;

namespace HRM.Model.Entities
{
    public class Employee_Shift : BaseEntity
    {
        public Guid Id { get; set; }
        public Guid ShiftId { get; set; }
        
        public virtual Employee Employee { get; set; }
        public virtual Shift Shift { get; set; }
    }
}