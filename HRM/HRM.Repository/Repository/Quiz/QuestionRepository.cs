﻿using HRM.Model;                    
using HRM.Model.Entities.HRM;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HRM.Repository.Repository.HRM
{
  public class QuestionRepository : Repository<Question>, IRepository<Question>
  {
    public QuestionRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
