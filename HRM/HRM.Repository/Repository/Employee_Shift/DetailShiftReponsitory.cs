﻿using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository.Employee_Shift
{
    public class DetailShiftReponsitory : Repository<Model.Entities.Employee_Shift>, IRepository<Model.Entities.Employee_Shift>
    {
        public DetailShiftReponsitory(DbContext dbContext) : base(dbContext)
        {
        }
    }
}