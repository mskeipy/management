﻿using HRM.Model.Entities.Employees;
using HRM.Model.ViewModel.Employees;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;
using Microsoft.EntityFrameworkCore;

namespace HRM.Repository.Repository.Employees
{
    public class EmployeeReponsitory : Repository<Employee>, IRepository<Employee>
    {
        public EmployeeReponsitory(DbContext dbContext) : base(dbContext)
        {
        }
    }
}