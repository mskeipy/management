﻿using HRM.Model;
using HRM.Model.Entities.Spaces;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HRM.Repository.Repository.Spaces
{
  public class DocumentRepository : Repository<Document>, IRepository<Document>
  {
    public DocumentRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
