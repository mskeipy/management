﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;

namespace HRM.Repository.Repository.Accounts
{
  public class AccountRepository : Repository<Account>, IRepository<Account>
  {
    public AccountRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
