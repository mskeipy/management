﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;

namespace HRM.Repository.Repository.Accounts
{
  public class ContactRepository : Repository<Contact>, IRepository<Contact>
  {
    public ContactRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
