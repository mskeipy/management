﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;

namespace HRM.Repository.Repository.Accounts
{
  public class AddressBookRepository : Repository<AddressBook>, IRepository<AddressBook>
  {
    public AddressBookRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
