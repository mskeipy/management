﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;

namespace HRM.Repository.Repository.Accounts
{
  public class IndustryRepository : Repository<Industry>, IRepository<Industry>
  {
    public IndustryRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
