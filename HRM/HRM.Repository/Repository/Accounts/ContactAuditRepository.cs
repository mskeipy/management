﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;

namespace HRM.Repository.Repository.Accounts
{
  public class ContactAuditRepository : Repository<ContactAudit>, IRepository<ContactAudit>
  {
    public ContactAuditRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
