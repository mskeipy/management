﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace HRM.Repository.Repository.Accounts
{
  public class AccountAuditRepository : Repository<AccountAudit>, IRepository<AccountAudit>
  {
    public AccountAuditRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
