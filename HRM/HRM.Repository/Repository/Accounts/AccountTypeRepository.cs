﻿using HRM.Model;
using HRM.Model.Entities.Accounts;
using HRM.UnitOfWork.Repositories;
using HRM.UnitOfWork.Shared;

namespace HRM.Repository.Repository.Accounts
{
  public class AccountTypeRepository : Repository<AccountType>, IRepository<AccountType>
  {
    public AccountTypeRepository(HRMDBContext dbContext) : base(dbContext)
    {

    }
  }
}
