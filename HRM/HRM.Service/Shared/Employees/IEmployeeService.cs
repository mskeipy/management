﻿using System;
using System.Threading.Tasks;
using HRM.Model.Entities.Employees;
using HRM.Model.ViewModel.Employees;
using HRM.Service.Collections;
using HRM.UnitOfWork.Collections;

namespace HRM.Service.Shared.Employees
{
    public interface IEmployeeService : IServiceBase<Employee, EmployeeViewModel>
    {
        EmployeeViewModel GetManager(Guid id);
        
    }
}