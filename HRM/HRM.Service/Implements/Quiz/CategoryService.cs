﻿//using Microsoft.EntityFrameworkCore;
//using HRM.Helper;
//using HRM.Model.Entities.HRM;
//using HRM.Service.Collections;
//using HRM.Service.Shared.HRM;
//using HRM.UnitOfWork.Collections;
//using HRM.UnitOfWork.Shared;
//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Threading.Tasks;
//
//namespace HRM.Service.Implements.HRM
//{
//  public class CategoryService : ServiceBase<Category>, IServiceBase<Category>, ICategoryService
//  {
//    IUnitOfWork _unitOfWork;
//    public CategoryService(IUnitOfWork unitOfWork) : base(unitOfWork)
//    {
//      _unitOfWork = unitOfWork;
//    }
//
//    public async Task<object> GetQuestions(Guid id)
//    {
//      var category = await _unitOfWork.GetRepository<Category>().GetFirstOrDefaultAsync(predicate: p => p.Id == id, include: c => c.Include(s => s.Spaces));
//      return Ultilities.ConvertModelToString(category);
//    }
//  }
//}
