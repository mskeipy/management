﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HRM.Model;
using HRM.Model.Entities.Employees;
using HRM.Model.ViewModel.Employees;
using HRM.Service.Collections;
using HRM.Service.Shared.Employees;
using HRM.UnitOfWork.Collections;
using HRM.UnitOfWork.Shared;
using Microsoft.Extensions.Logging;
using Serilog;

namespace HRM.Service.Implements.Employees
{
    public class EmployeeService : ServiceBase<Employee, EmployeeViewModel>, IServiceBase<Employee, EmployeeViewModel>,
        IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly HRMDBContext _context;
        private readonly ILogger<EmployeeService> _logger;

        public EmployeeService(IUnitOfWork unitOfWork,
            IMapper mapper, HRMDBContext context, ILogger<EmployeeService> logger) : base(unitOfWork, mapper, logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _context = context;
            _logger = logger;
        }

        public EmployeeViewModel GetManager(Guid id)
        {
            var employee = Get(id);
            var manager = _unitOfWork.GetRepository<Employee>()
                .GetFirstOrDefault(predicate: e => e.UnitId.Equals(employee.UnitId) && e.IsManager && !e.IsDelete);
            return _mapper.Map<EmployeeViewModel>(manager);
        }
    }
}